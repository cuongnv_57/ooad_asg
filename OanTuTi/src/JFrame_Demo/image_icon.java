package JFrame_Demo;

import javax.swing.ImageIcon;

public class image_icon {
	private String ComRock = "image/ComRock.png";
	private String ComPaper = "image/ComPaper.png";
	private String ComSciss= "image/ComScissors.png";
	private String HumRock = "image/HumRock.png";
	private String HumPaper = "image/HumPaper.png";
	private String HumSciss = "image/HumScissors.png";
	
	public ImageIcon Humimage(int choice){
		if(choice == 1) return new ImageIcon(HumRock);
		if(choice == 2) return new ImageIcon(HumPaper);
		if(choice == 3) return new ImageIcon(HumSciss);
		return null;
	}
	public ImageIcon Comimage(int choice){
		if(choice == 1) return new ImageIcon(ComRock);
		if(choice == 2) return new ImageIcon(ComPaper);
		if(choice == 3) return new ImageIcon(ComSciss);
		return null;
	}
}
