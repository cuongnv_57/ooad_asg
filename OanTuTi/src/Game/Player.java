package Game;

public abstract class Player {
	protected  int point;
	private static int pointTies;
	protected int Choice;
	
	public Player(){
		point = 0;
		pointTies = 0;
		Choice = 0;
	}
	
	public abstract int play();
	
	public void setChoice(int choice){
		Choice = choice;
	}
	
	public int getChoice(){
		return Choice;
	}
	public int getpoint(){
		return point;
	}
	public void Win(){
		point++;
	}
	public void Ties(){
		pointTies++;
	}
	public int getpointTies(){
		return pointTies;
	}
}
