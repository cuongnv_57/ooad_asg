package Game;

import java.util.ArrayList;
import java.util.Random;

public class Computer extends Player{
	
	boolean isModeAI;
	String arrayChoice;
	public Computer(boolean model){
		super();
		isModeAI = model;
		arrayChoice = "";
	}
	public Computer(){
		super();
		isModeAI = false;
		arrayChoice = "";
	}
	
	Random rd = new Random(); 
	public int random(){
		return rd.nextInt(3) + 1;
	}
	
	public int playfindChoice4Com(String arrayChoice) {
		int popularHumanChoice = 0;
		if (arrayChoice.length() >= 3) {
			int index = 0;
			ArrayList<String> arrayMatch = new ArrayList<String>();
			String popularMatch = "";
			String lastMatch = "";
			lastMatch = arrayChoice.substring(arrayChoice.length() - 2);
			while (index < (arrayChoice.length() - 2)) {
				if (arrayChoice.indexOf(lastMatch, index) != -1) {
					int pos = arrayChoice.indexOf(lastMatch, index);
					if (pos + 3 <= arrayChoice.length() && (pos % 2) == 0)
						arrayMatch.add(arrayChoice.substring(pos, pos + 3));
					index = pos + 3;
				}
			}

			if (!arrayMatch.isEmpty()) {
				popularMatch = findPopularString(arrayMatch);
				popularHumanChoice = Integer
						.parseInt(popularMatch.substring(2));
			}
		}

		if (popularHumanChoice == 1)
			return 2;
		else if (popularHumanChoice == 2)
			return 3;
		else if (popularHumanChoice == 3)
			return 1;
		else
			return 0;

	}

	public String findPopularString(ArrayList<String> arrayString) {
		int count = 0;
		int index = 0;

		for (int i = 0; i < arrayString.size() - 1; i++) {
			int mCount = 0;
			for (int j = i + 1; j < arrayString.size(); j++) {
				if (arrayString.get(i).equals(arrayString.get(j))) {
					mCount++;
				}
			}

			if (mCount >= count) {
				index = i;
				count = mCount;
			}
		}

		return arrayString.get(index);
	}
	
	public int play(){
		if (isModeAI) {
			int Comfind = playfindChoice4Com(arrayChoice);
			if(Comfind != 0){
			Choice = Comfind;
		}else{
			Choice = random();
		}
		} else {
			Choice = random();
		}
		return Choice;
		
	}
	public void setarraychoice(String array){
		arrayChoice = array;
	}
}
