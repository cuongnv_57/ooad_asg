package Game;

import java.awt.Window;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;


public class Help extends JDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Help(Window owner){
		super(owner, "Help", ModalityType.APPLICATION_MODAL);
		
		String html = "How to play this game?\n</em><br/>"
				+ "This game is played by you and computer,</em><br/> "
				+ "where players simultaneously form one of</em><br/> " 
				+"three shapes with an outstretched hand.</em><br/> "
				+ "The rock beats scissors, the scissors</em><br/>"
				+"beat paper and the paper beats rock;</em><br/> "
				+ "if both players throw the same shape, the game is tied\n</em><br/>"
				+ "If you want to chose rock, you type 0;</em><br/>"
				+ "if you want to chose scissor, you type 1;</em><br/>"
				+ "and if you want to chose paper, you type 2\n</em><br/>"
				+ "Moreover you can stop game and start again if</em><br/>"
				+" you don't want to continue!\n</em><br/>"
				+ "Good luck for you!!!\n </em><br/>";

		Box b = Box.createVerticalBox();
		JEditorPane ep = new JEditorPane("text/html", html);
		ep.setEditable(false);
		ep.setBackground(getBackground());
		b.add(ep);

		// TODO: Create the button "OK" is center aligned
		JButton btn = new JButton("OK");
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				submit();
			}
		});
		btn.setAlignmentX(0.5f);
		b.add(btn);

		int w = 10;
		b.setBorder(BorderFactory.createEmptyBorder(w, w, w, w));
		add(b);

		pack();
		setLocationRelativeTo(null);
	}

	protected void submit() {
		dispose();
	}

}
