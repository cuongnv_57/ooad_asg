package Game;

public class ResultofMatch {
	public int rock = 1;
	public int paper = 2;
	public int scissors = 3;
	

	public void result(Player pl1, Player pl2) {
		int pHuman = pl1.getChoice();
		int pCom = pl2.getChoice();
		switch (pHuman) {
		case 1:
			if (pCom == 1) {
				pl2.Ties();
				break;
			}
			if (pCom == 2) {
				pl2.Win();
				break;
			}
			if (pCom == 3) {
				pl1.Win();
				break;
			}
		case 2:
			if (pCom == 1) {
				pl1.Win();
				break;
			}
			if (pCom == 2) {
				pl2.Ties();
				break;
			}
			if (pCom == 3) {
				pl2.Win();
				break;
			}
		case 3:
			if (pCom == 1) {
				pl2.Win();
				break;
			}
			if (pCom == 2) {
				pl1.Win();
				break;
			}
			if (pCom == 3) {
				pl2.Ties();
				break;
			}
		}
	}
}
